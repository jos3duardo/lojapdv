<nav class="col-lg-2 col-md-3">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a href=""><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('Edit {0}', ['Caixa']), ['action' => 'edit', $caixa->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete {0}', ['Caixa']), ['action' => 'delete', $caixa->id], ['confirm' => __('Are you sure you want to delete # {0}?', $caixa->id)]) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Caixas']), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Caixa']), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="caixas view col-lg-10 col-md-9">
    <h3><?= h($caixa->id) ?></h3>
    <table class="table table-striped table-hover">
        <tr>
            <th>Nome</th>
            <td><?= h($caixa->nome) ?></td>
        </tr>
        <tr>
            <th>'Id</th>
            <td><?= $this->Number->format($caixa->id) ?></td>
        </tr>
        <tr>
            <th>'Entrada</th>
            <td><?= $this->Number->format($caixa->entrada) ?></td>
        </tr>
        <tr>
            <th>'Fechamento</th>
            <td><?= $this->Number->format($caixa->fechamento) ?></td>
        </tr>
        <tr>
            <th>Created</th>
            <td><?= h($caixa->created) ?></tr>
        </tr>
        <tr>
            <th>Modified</th>
            <td><?= h($caixa->modified) ?></tr>
        </tr>
    </table>
</div>
