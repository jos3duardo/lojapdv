<div class="row">
<nav class="col-md-2" id="actions-sidebar">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('New {0}', ['Caixa']), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="caixas index col-md-10 columns content">
    <h3>Caixas</h3>
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('entrada') ?></th>
                <th><?= $this->Paginator->sort('fechamento') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($caixas as $caixa): ?>
            <tr>
                <td><?= $this->Number->format($caixa->id) ?></td>
                <td><?= h($caixa->nome) ?></td>
                <td><?= $this->Number->format($caixa->entrada) ?></td>
                <td><?= $this->Number->format($caixa->fechamento) ?></td>
                <td><?= h($caixa->created) ?></td>
                <td><?= h($caixa->modified) ?></td>
                <td class="actions" style="white-space:nowrap">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $caixa->id], ['class'=>'btn btn-success btn-xs']) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $caixa->id], ['class'=>'btn btn-primary btn-xs']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $caixa->id], ['confirm' => __('Are you sure you want to delete # {0}?', $caixa->id), 'class'=>'btn btn-danger btn-xs']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <center>
            <ul class="pagination">
                <?= $this->Paginator->prev('&laquo; ' . __('previous'), ['escape'=>false]) ?>
                <?= $this->Paginator->numbers(['escape'=>false]) ?>
                <?= $this->Paginator->next(__('next') . ' &raquo;', ['escape'=>false]) ?>
            </ul>
            <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} records out of
         {{count}} total, starting on record {{start}}, ending on {{end}}')) ?></p>
        </div>
    </center>
</div>
</div>