<nav class="col-md-2 columns" id="actions-sidebar">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('List {0}', 'Caixas'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="caixas form col-md-10 columns content">
    <?= $this->Form->create($caixa) ?>
    <fieldset>
        <legend><?= 'Add Caixa' ?></legend>
        <?php
            echo $this->Form->input('nome');
            echo $this->Form->input('entrada');
            echo $this->Form->input('fechamento');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
