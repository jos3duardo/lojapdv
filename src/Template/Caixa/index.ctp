<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Caixa[]|\Cake\Collection\CollectionInterface $caixa
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Caixa'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="caixa index large-9 medium-8 columns content">
    <h3><?= __('Caixa') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nome') ?></th>
                <th scope="col"><?= $this->Paginator->sort('entrada') ?></th>
                <th scope="col"><?= $this->Paginator->sort('fechamento') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($caixa as $caixa): ?>
            <tr>
                <td><?= $this->Number->format($caixa->id) ?></td>
                <td><?= h($caixa->nome) ?></td>
                <td><?= $this->Number->format($caixa->entrada) ?></td>
                <td><?= $this->Number->format($caixa->fechamento) ?></td>
                <td><?= h($caixa->created) ?></td>
                <td><?= h($caixa->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $caixa->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $caixa->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $caixa->id], ['confirm' => __('Are you sure you want to delete # {0}?', $caixa->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
