<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Caixa $caixa
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Caixa'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="caixa form large-9 medium-8 columns content">
    <?= $this->Form->create($caixa) ?>
    <fieldset>
        <legend><?= __('Add Caixa') ?></legend>
        <?php
            echo $this->Form->control('nome');
            echo $this->Form->control('entrada');
            echo $this->Form->control('fechamento');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
