      
<script>

// Exemplo de requisição GET
var ajax = new XMLHttpRequest();

// Seta tipo de requisição e URL com os parâmetros
ajax.open("GET", "https://www.receitaws.com.br/v1/cnpj/13150088000170");

// Envia a requisição
ajax.send();

// Cria um evento para receber o retorno.
ajax.onreadystatechange = function() {
  // Caso o state seja 4 e o http.status for 200, é porque a requisiçõe deu certo.
	if (ajax.readyState == 4 && ajax.status == 200) {
		var resultado = ajax.responseText;
    // Retorno do Ajax
        console.log(resultado);
       var $dadosJson = json_decode($resultado, TRUE);

	}
}



</script>

<?php

    $cnpj = $_POST['cnpj'];
    $url = "https://www.receitaws.com.br/v1/cnpj/";

    //concatena o cnpj com o link para consulta
    $resultado = $url.$cnpj;

    $retorno = file_get_contents($resultado);
    $dadosJson = json_decode($retorno, TRUE);
    
    //echo $retorno;
?>


        <br>
        <h2 class="text-center"><b>Dados da Empresa</b></h2>
        <hr>
        <form>
            <fieldset disabled>
            <div class="row">
                    <div class="col-md-6">
                        <label for="disabledTextInput">Razão Social</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['nome'] ?>">
                    </div>
                    <div class="col-md-4">
                        <label for="disabledTextInput">Nome Fantasia</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['fantasia'] ?>">
                    </div>
                    <div class="col-md-2">
                        <label for="disabledTextInput">Situação</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['situacao'] ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label for="disabledTextInput">Número do CNPJ</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['cnpj']?>">
                    </div>
                    <div class="col-md-2">
                        <label for="disabledTextInput">Início das atividades</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['abertura'] ?>">
                    </div>
                    <div class="col-md-4">
                        <label for="disabledTextInput">Natureza Jurídica</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['natureza_juridica'] ?>">
                    </div>
                    <div class="col-md-2">
                        <label for="disabledTextInput">Tipo</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['tipo'] ?>">
                    </div>
                    <div class="col-md-2">
                        <label for="disabledTextInput">Capital Social</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo number_format($dadosJson['capital_social'], 2, ',', '.')?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="disabledTextInput">Logradouro</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['logradouro'] ?>">
                    </div>
                    <div class="col-md-2">
                        <label for="disabledTextInput">Número</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['numero'] ?>">
                    </div>
                    <div class="col-md-4">
                        <label for="disabledTextInput">Complemento</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['complemento'] ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="disabledTextInput">Bairro</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['bairro'] ?>">
                    </div>
                    <div class="col-md-2">
                        <label for="disabledTextInput">CEP</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['cep'] ?>">
                    </div>
                    <div class="col-md-4">
                        <label for="disabledTextInput">Município</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['municipio'] ?>">
                    </div>
                    <div class="col-md-2">
                        <label for="disabledTextInput">UF</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['uf'] ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="disabledTextInput">Telefone</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['telefone'] ?>">
                    </div>
                    <div class="col-md-6">
                        <label for="disabledTextInput">Email</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['email'] ?>">
                    </div>
                </div>
                <br>
                <h2 class="text-center"><b>Ramo de Atuação</b></h2><br>
                <h4><b>Atividade Econômica Principal</b></h4>
                <div class="row">
                    <div class="col-md-2">
                        <label for="disabledTextInput">Código</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['atividade_principal'][0]['code'] ?>">
                    </div>
                    <div class="col-md-10">
                        <label for="disabledTextInput">Descrição</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['atividade_principal'][0]['text'] ?>">
                    </div>
                </div>
                <br>
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th scope="col">Código</th>
                        <th scope="col">Atividade(s) Econômica(s) Secundaria(s)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($dadosJson['atividades_secundarias'] as $atividades){
                            echo "<tr>";
                            echo "<th>".$atividades['code']."</th>";
                            echo "<td>".$atividades['text']."</td>";    
                            echo "</tr>";
                        }?>
                    </tbody>
                </table>
                <br>
                <h2 class="text-center"><b>Quadro de Sócios e Administradores</b></h2>
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th scope="col">Qualificação</th>
                        <th scope="col">Nome</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($dadosJson['qsa'] as $socio){
                            echo "<tr>";
                            echo "<th>".$socio['qual']."</th>";
                            echo "<td>".$socio['nome']."</td>";    
                            echo "</tr>";
                        }?>
                    </tbody>
                </table>
            </fieldset>
        </form>  
    </div>
