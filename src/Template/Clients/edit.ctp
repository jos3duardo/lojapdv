<nav class="col-md-2 columns" id="actions-sidebar">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a><?= __('Actions') ?></a></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $client->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $client->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Clients'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="clients form col-md-10 columns content">
    <?= $this->Form->create($client) ?>
    <fieldset>
        <legend><?= 'Edit Client' ?></legend>
        <?php
            echo $this->Form->input('razao_social');
            echo $this->Form->input('nome_fantasia');
            echo $this->Form->input('cnpj');
            echo $this->Form->input('inicio_atividades');
            echo $this->Form->input('tipo');
            echo $this->Form->input('logradouro');
            echo $this->Form->input('numero');
            echo $this->Form->input('bairro');
            echo $this->Form->input('cep');
            echo $this->Form->input('municipio');
            echo $this->Form->input('uf');
            echo $this->Form->input('telefone');
            echo $this->Form->input('celuar');
            echo $this->Form->input('email');
            echo $this->Form->input('contato');
            echo $this->Form->input('codigo');
            echo $this->Form->input('descricao');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>










</div>
