<nav class="col-lg-2 col-md-3">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a href=""><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('Edit {0}', ['Client']), ['action' => 'edit', $client->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete {0}', ['Client']), ['action' => 'delete', $client->id], ['confirm' => __('Are you sure you want to delete # {0}?', $client->id)]) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Clients']), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Client']), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="clients view col-lg-10 col-md-9">
    <h3><?= h($client->id) ?></h3>
    <table class="table table-striped table-hover">
        <tr>
            <th>Razao Social</th>
            <td><?= h($client->razao_social) ?></td>
        </tr>
        <tr>
            <th>Nome Fantasia</th>
            <td><?= h($client->nome_fantasia) ?></td>
        </tr>
        <tr>
            <th>Inicio Atividades</th>
            <td><?= h($client->inicio_atividades) ?></td>
        </tr>
        <tr>
            <th>Tipo</th>
            <td><?= h($client->tipo) ?></td>
        </tr>
        <tr>
            <th>Logradouro</th>
            <td><?= h($client->logradouro) ?></td>
        </tr>
        <tr>
            <th>Numero</th>
            <td><?= h($client->numero) ?></td>
        </tr>
        <tr>
            <th>Bairro</th>
            <td><?= h($client->bairro) ?></td>
        </tr>
        <tr>
            <th>Cep</th>
            <td><?= h($client->cep) ?></td>
        </tr>
        <tr>
            <th>Municipio</th>
            <td><?= h($client->municipio) ?></td>
        </tr>
        <tr>
            <th>Uf</th>
            <td><?= h($client->uf) ?></td>
        </tr>
        <tr>
            <th>Telefone</th>
            <td><?= h($client->telefone) ?></td>
        </tr>
        <tr>
            <th>Celuar</th>
            <td><?= h($client->celuar) ?></td>
        </tr>
        <tr>
            <th>Email</th>
            <td><?= h($client->email) ?></td>
        </tr>
        <tr>
            <th>Contato</th>
            <td><?= h($client->contato) ?></td>
        </tr>
        <tr>
            <th>Codigo</th>
            <td><?= h($client->codigo) ?></td>
        </tr>
        <tr>
            <th>Descricao</th>
            <td><?= h($client->descricao) ?></td>
        </tr>
        <tr>
            <th>'Id</th>
            <td><?= $this->Number->format($client->id) ?></td>
        </tr>
        <tr>
            <th>'Cnpj</th>
            <td><?= $this->Number->format($client->cnpj) ?></td>
        </tr>
        <tr>
            <th>Created</th>
            <td><?= h($client->created) ?></tr>
        </tr>
        <tr>
            <th>Modified</th>
            <td><?= h($client->modified) ?></tr>
        </tr>
    </table>
</div>
