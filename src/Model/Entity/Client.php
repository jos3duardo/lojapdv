<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Client Entity
 *
 * @property int $id
 * @property string $razao_social
 * @property string $nome_fantasia
 * @property int $cnpj
 * @property string $inicio_atividades
 * @property string $tipo
 * @property string $logradouro
 * @property string $numero
 * @property string $bairro
 * @property string $cep
 * @property string $municipio
 * @property string $uf
 * @property string $telefone
 * @property string $celuar
 * @property string $email
 * @property string $contato
 * @property string $codigo
 * @property string $descricao
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class Client extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'razao_social' => true,
        'nome_fantasia' => true,
        'cnpj' => true,
        'inicio_atividades' => true,
        'tipo' => true,
        'logradouro' => true,
        'numero' => true,
        'bairro' => true,
        'cep' => true,
        'municipio' => true,
        'uf' => true,
        'telefone' => true,
        'celuar' => true,
        'email' => true,
        'contato' => true,
        'codigo' => true,
        'descricao' => true,
        'created' => true,
        'modified' => true
    ];
}
