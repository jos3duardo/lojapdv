<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Caixa Model
 *
 * @method \App\Model\Entity\Caixa get($primaryKey, $options = [])
 * @method \App\Model\Entity\Caixa newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Caixa[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Caixa|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Caixa|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Caixa patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Caixa[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Caixa findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CaixaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('caixa');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 512)
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->integer('entrada')
            ->requirePresence('entrada', 'create')
            ->notEmpty('entrada');

        $validator
            ->integer('fechamento')
            ->requirePresence('fechamento', 'create')
            ->notEmpty('fechamento');

        return $validator;
    }
}
