<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Clients Model
 *
 * @method \App\Model\Entity\Client get($primaryKey, $options = [])
 * @method \App\Model\Entity\Client newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Client[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Client|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Client|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Client patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Client[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Client findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ClientsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('clients');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('razao_social')
            ->maxLength('razao_social', 512)
            ->requirePresence('razao_social', 'create')
            ->notEmpty('razao_social');

        $validator
            ->scalar('nome_fantasia')
            ->maxLength('nome_fantasia', 512)
            ->requirePresence('nome_fantasia', 'create')
            ->notEmpty('nome_fantasia');

        $validator
            ->integer('cnpj')
            ->requirePresence('cnpj', 'create')
            ->notEmpty('cnpj');

        $validator
            ->scalar('inicio_atividades')
            ->maxLength('inicio_atividades', 100)
            ->requirePresence('inicio_atividades', 'create')
            ->notEmpty('inicio_atividades');

        $validator
            ->scalar('tipo')
            ->maxLength('tipo', 100)
            ->requirePresence('tipo', 'create')
            ->notEmpty('tipo');

        $validator
            ->scalar('logradouro')
            ->maxLength('logradouro', 100)
            ->requirePresence('logradouro', 'create')
            ->notEmpty('logradouro');

        $validator
            ->scalar('numero')
            ->maxLength('numero', 10)
            ->requirePresence('numero', 'create')
            ->notEmpty('numero');

        $validator
            ->scalar('bairro')
            ->maxLength('bairro', 512)
            ->requirePresence('bairro', 'create')
            ->notEmpty('bairro');

        $validator
            ->scalar('cep')
            ->maxLength('cep', 10)
            ->requirePresence('cep', 'create')
            ->notEmpty('cep');

        $validator
            ->scalar('municipio')
            ->maxLength('municipio', 100)
            ->requirePresence('municipio', 'create')
            ->notEmpty('municipio');

        $validator
            ->scalar('uf')
            ->maxLength('uf', 2)
            ->requirePresence('uf', 'create')
            ->notEmpty('uf');

        $validator
            ->scalar('telefone')
            ->maxLength('telefone', 100)
            ->requirePresence('telefone', 'create')
            ->notEmpty('telefone');

        $validator
            ->scalar('celuar')
            ->maxLength('celuar', 100)
            ->requirePresence('celuar', 'create')
            ->notEmpty('celuar');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->scalar('contato')
            ->maxLength('contato', 100)
            ->requirePresence('contato', 'create')
            ->notEmpty('contato');

        $validator
            ->scalar('codigo')
            ->maxLength('codigo', 100)
            ->requirePresence('codigo', 'create')
            ->notEmpty('codigo');

        $validator
            ->scalar('descricao')
            ->maxLength('descricao', 512)
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }
}
